package unidad4;
import java.util.Random;
import java.util.Scanner;

public class Ejercicio6_3 {

		
	public static void main(String[] args) {
		
	
		int[] vector = new int [5];
		llenado(vector);
		invertir(vector);
		imprimeVector(vector);
		}
	
	public static int[] llenado(int[] vector) {
		for(int i=0; i<vector.length; i ++) {
			vector[i] =  new Random().nextInt(-100 + 300 + 1)-100;
		}
		return vector;
	}
	public static int [] invertir(int [] vector){
		int vuelta;
		for(int i = 0; i < vector.length / 2; i++) {
			vuelta = vector[i];
			vector[i] = vector[vector.length - 1 - i];
			vector[vector.length - 1 - i] = vuelta;
		}
		return vector;
	}
	public static void imprimeVector(int[] vector) {
		
	for(int i=0; i<vector.length; i ++) {
		System.out.print(String.valueOf(vector[i]) + " ");
		
	}
	}

}
