package unidad4;

import java.util.Scanner;

public class ejercicio3 {
	
public static void main(String[] args) {
	Scanner in = new Scanner(System.in);
	System.out.println("Dime una frase");
	String a = in.nextLine();
	System.out.println("Dime otra frase");
	String b = in.nextLine();
	int i = 0;
	int contador = 0;
	if (a.length() < b.length())
		System.out.println("La segunda cadena no puede ser más larga que la primera");
	else {
		do {
			i = a.indexOf(b, i);
			if (i != -1) {
				i++;
				contador++;
			}
		} while (i != -1);
		System.out.println("La segunda está contenida " + contador + " veces en la primera");
	}
	in.close();
}

}
