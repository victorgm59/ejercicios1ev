package Unidad3;

import java.util.Scanner;

public class Calculadora {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		char caracter;
		System.out.println("Que operacion desea realizar");
		System.out.println("+ = sumar, - = restar, * = multiplicar, / = dividir, s = salir)");
		String respuesta = in.nextLine();
		caracter = respuesta.charAt(0);
		
		float resultado = 0;
		float n1;
		float n2;
		while (respuesta.equalsIgnoreCase("s")) {
			System.out.println("Dime el primer numero");
			n1 = in.nextFloat();
			System.out.println("Dime el segundo numero");
			n2 = in.nextFloat();
			
			switch (respuesta) {
			case "+":
				resultado = n1 + n2;
				break;
			case "-":
				resultado = n1 - n2;
				break;
			case "*":
				resultado = n1 * n2;
				break;
			case "/":
				resultado = n1 / n2;
				break;
			}
			System.out.println(n1 + respuesta + n2 + " = " + resultado);

			System.out.println("Deseas realizar otra operacion: ");
			respuesta = in.nextLine();
		}

	}
}
