package Unidad3;

import java.util.Scanner;

public class Hora {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduce la hora");
		int hora = entrada.nextInt();
		System.out.println("Introduce los minutos");
		int minutos = entrada.nextInt();
		System.out.println("Introduce los segundos");
		int segundos = entrada.nextInt();
		segundos++;
		if(segundos > 59) {
			segundos = 0;
			minutos++;
			if(minutos > 59) {	
				minutos = 0;
				hora++;
				if(hora >23) 
					hora = 0;
			}		
		}
		System.out.println(hora + ":" + minutos + ":" + segundos);
	}

}
