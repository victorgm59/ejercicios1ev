package Unidad3;

import java.util.Scanner;

public class Multiplicar {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		System.out.println("Deseas repasar una tabla (s/n): ");
		String respuesta = in.nextLine();
		int numero;
		int resultado;
		int resultadocorrecto;
		int fallos;
		while (respuesta.equalsIgnoreCase("s")) {
			System.out.println("Que tabla deseas repasar: ");
			numero = Integer.parseInt(in.nextLine());
			fallos = 0;
			for(int i = 1; i < 11; i ++) {
				resultadocorrecto = numero * i;
				System.out.print(numero + "*" + i + "= ");
				resultado = Integer.parseInt(in.nextLine());
				if(resultado == resultadocorrecto) {
					System.out.println("ok");
					}
				else {
					fallos++;
					System.out.println("no ok, el resultado es " + resultadocorrecto);
					}
				}
				if(fallos < 2) {
					
					System.out.println("Aprobado");
				}
				else {
					System.out.println("Suspendido");
				}
			System.out.println("Deseas repasar otra tabla (s/n): ");
			respuesta = in.nextLine();	
		}
	}
}
