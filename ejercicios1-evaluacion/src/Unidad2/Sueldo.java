package Unidad2;

import java.util.Scanner;

public class Sueldo {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		System.out.println("Sueldo base: ");
		float sueldo= in.nextFloat();
		double comision = (sueldo * 0.1) * 3;
		System.out.println("Mis comisiones son: " + comision + " euros");
		System.out.println("Mi nomina es: " + (sueldo + comision) + " euros");
	}

}
