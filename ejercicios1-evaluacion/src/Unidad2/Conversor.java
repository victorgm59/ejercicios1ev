package Unidad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Conversor {

	public static void main(String[] args) throws NumberFormatException, IOException {
		float dolar = 1.17f;
		float euro = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Introduce euros :");
		euro = Float.parseFloat(br.readLine());
		System.out.printf("Dolares:%.2f", dolar*euro);
	}

}