package Unidad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calificaciones {
	
	public static void main(String[] args) throws NumberFormatException, IOException {	
	
		float examenM;
		float tarea1M;
		float tarea2M;
		float tarea3M;
		
		float examenF;
		float tarea1F;
		float tarea2F;
		
		float examenQ;
		float tarea1Q;
		float tarea2Q;
		float tarea3Q;
				
		BufferedReader in = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Nota examen matemáticas: ");
		examenM = Integer.parseInt(in.readLine());
		System.out.println("Nota primera tarea matemáticas: ");
		tarea1M = Integer.parseInt(in.readLine());
		System.out.println("Nota segunda tarea matematicas: ");
		tarea2M = Integer.parseInt(in.readLine());
		System.out.println("Nota tercera tarea matemáticas: ");
		tarea3M = Integer.parseInt(in.readLine());
		float notaM = examenM * 0.9f + ((tarea1M + tarea2M + tarea3M) /3) * 0.1f;
		
		System.out.println("Nota examen física: ");
		examenF = Integer.parseInt(in.readLine());
		System.out.println("Nota primera tarea física: ");
		tarea1F = Integer.parseInt(in.readLine());
		System.out.println("Nota segunda tarea física: ");
		tarea2F = Integer.parseInt(in.readLine());
		float notaF = examenF * 0.8f + ((tarea1F + tarea2F) /2) * 0.2f;
		
		System.out.println("Nota examen química: ");
		examenQ = Integer.parseInt(in.readLine());
		System.out.println("Nota primera tarea química: ");
		tarea1Q = Integer.parseInt(in.readLine());
		System.out.println("Nota segunda tarea química: ");
		tarea2Q = Integer.parseInt(in.readLine());
		System.out.println("Nota tercera tarea química: ");
		tarea3Q = Integer.parseInt(in.readLine());
		float notaQ = examenQ * 0.85f + ((tarea1Q + tarea2Q + tarea3Q) /3) * 0.15f;

		System.out.printf("Nota media final Matemáticas: %.2f \n" , notaM);
		System.out.printf("Nota media final Física: %.2f \n" , notaF);
		System.out.printf("Nota media final Química: %.2f \n" , notaQ);
		
	}
}
