package Unidad2;

import java.util.Scanner;

public class color {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		System.out.println("Introduzca color rojo: ");
		float rojo= in.nextFloat();
		System.out.println("Introduzca color verde: ");
		float verde= in.nextFloat();
		System.out.println("Introduzca color azul: ");
		float azul= in.nextFloat();
		float y = rojo * 0.299f + verde * 0.587f + azul * 0.114f;
		float i = rojo * 0.596f - verde * 0.275f - azul * 0.321f;
		float q = rojo * 0.212f - verde * 0.528f + azul * 0.311f;
		
		System.out.printf("El componente y es: %.3f \n", y);
		System.out.printf("El componente i es: %.3f \n", i);
		System.out.printf("El componente q es: %.3f \n", q);
	}

}
